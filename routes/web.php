<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/activation/{token}', 'Auth\RegisterController@activateUser')->name('user.activate');
Route::post('/resend_confirmation_email', 'Auth\RegisterController@resendConfirmationEmail');
Route::get('/user/activation/{token}', 'Auth\RegisterController@activateUser')->name('user.activate');
Route::group(['middleware' => ['inactive_user']], function () {

    Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);

    Route::get('/confirmation', function (){
        return view('confirmation');
    })->middleware('auth');

});
Route::get('/activation', function() {
    if ( \Illuminate\Support\Facades\Auth::user()->activated )
    {
        return redirect('home');
    }

    return view('activation');
});

Route::get('task', ['as' => 'task.index','middleware' => 'auth', 'uses' => 'TaskController@index']);
Route::get('tasks', ['as' => 'task.my','middleware' => 'auth', 'uses' => 'TaskController@myTasks']);
Route::get('task/create', ['as' => 'task.create','middleware' => 'role:admin', 'uses' => 'TaskController@create']);
Route::get('task/{id}/edit', ['as' => 'task.edit','uses' => 'TaskController@edit']);
Route::get('task/{id}', ['as' => 'task.show','uses' => 'TaskController@show']);
Route::post('task', ['as' => 'task.store','middleware' => 'role:admin','uses' => 'TaskController@store']);
Route::patch('task/{id}', ['as' => 'task.update','uses' => 'TaskController@update']);
Route::delete('task/{id}', ['as' => 'task.destroy','middleware' => 'role:admin','uses' => 'TaskController@destroy']);