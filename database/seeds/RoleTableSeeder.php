<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
           [
               'name' => 'developer',
               'display_name' => 'development',
               'description' => 'read task and change progress task'
           ],
            [
                'name' => 'admin',
                'display_name' => 'Administrator',
                'description' => 'read,create,edit and delete task.task unit to developers'
            ]
        ]);
    }
}
