<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserActivations extends Model
{
    protected $table = 'user_activations';

    protected $fillable = ['user_id', 'token'];

    public $timestamps = ["created_at"];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
