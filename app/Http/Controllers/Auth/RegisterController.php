<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\ActivationService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Role;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    const ACTIVATED = 0;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $activationService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(ActivationService $activationService)
    {
        $this->middleware('guest', ['except' => ['resendConfirmationEmail', 'activateUser']]);
        $this->activationService = $activationService;
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->create($request->all());

        $this->activationService->sendActivationMail($user);

        Auth::loginUsingId($user->id);
//        return redirect('/login')->with('status', 'We sent you an activation code. Check your email.');
        return redirect('/activation');
    }

    public function authenticated(Request $request, $user)
    {
        if (!$user->activated) {
            $this->activationService->sendActivationMail($user);
            //auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        return redirect()->intended($this->redirectPath());
    }

    public function activateUser($token)
    {
        return $this->activationService->activateUser($token) ? redirect('/confirmation') : redirect('/login')->with('warning', 'Something went wrong...');
    }

    public function resendConfirmationEmail()
    {
        $token = Auth::user()->token()->first();

        if (time()-strtotime($token->created_at)<30) {
            return response()->json(['status' => 'danger','message' => 'Try again in a few moments.']);
        }
//
        $this->activationService->sendActivationMail(Auth::user());
//
        return response()->json(['status' => 'success', 'message' => 'Email has been sent.']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user=User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'activated' => self::ACTIVATED
        ]);
        $role = Role::where('name',$data[ 'optradio' ])->first();
        $user->attachRole($role);
        return $user;
    }
}
