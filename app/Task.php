<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Task extends Model
{
    protected $table = 'task';

    public function users()
    {
        $this->belongsToMany('App\User','user_task');
    }

    public function status()
    {
        return DB::table('progress')->where('id',$this->id)->first()->name;
    }

    public function belongs(User $user)
    {
        if(isset($user->tasks()->where('id',$this->id)->first)) {
            return true;
        }
        return false;
    }
}
