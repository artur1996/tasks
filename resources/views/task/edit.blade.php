@extends('layouts.app')

@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/additional-methods.min.js"></script>
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <section class="panel">
                    <div class="panel-body">
                        <form action="" method="get" class="form-horizontal form-bordered" name="edit-task" id="#task">
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-6">
                                    <div class="alert alert-info collapse">

                                    </div>
                                </div>
                            </div>
                            @role('admin')
                            <div class="form-group">
                                <label class="col-md-3 control-label">Task Name</label>
                                <div class="col-md-6">
                                    <input type="text" value="{{ $task->name }}" maxlength="100"
                                           class="form-control" name="name" id="name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Task Description</label>
                                <div class="col-md-6">
                                    <input type="text" value="{{ $task->description }}"  maxlength="100" class="form-control" name="description" id="description" required>
                                </div>
                            </div>
                            @endrole
                            <div class="form-group">
                                <label class="col-md-3 control-label">Task Status</label>
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label><input type="radio" name="progress" value="1" class="progress">Not Started</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="progress" value="2" class="progress">Progressing</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="progress" value="3" class="progress">Completed</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-6">
                                    <button type="button"
                                            class="btn btn-primary btn-lg mb-xlg" id="updateTask">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#updateTask").click(function () {
                var name = $('#name').val(),
                    description = $('#description').val(),
                    progress = $('.progress:checked').val();

                    $.ajax({
                        url: ' {{ route('task.update',$task->id) }}',
                        type: 'patch',
                        dataType: "json",
                        data: {name: name, description: description, progress:progress},
                        success: function (result) {
                            var alert = $("div.alert.alert-info");
                            alert.html(result).show();
                            window.scrollTo(0, 0);
                        }
                    });
            });

        });
    </script>
@endsection