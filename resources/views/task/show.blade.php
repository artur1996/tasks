@extends('layouts.app')
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-5 col-lg-4">
                    <section class="panel">
                        <div class="panel-body">
                            <div class="thumb-info mb-md">
                                <img src="/img/!logged-user.jpg" class="rounded img-responsive" alt="John Doe">
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-7">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#personalInfo" data-toggle="tab">Personal Information</a></li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="personalInfo">
                            <div class="icons">
                                @if($task->belongs(\Illuminate\Support\Facades\Auth::user()))
                                    <a class="pull-right icon"
                                       data-toggle="tooltip" data-placement="top" title="Your task" style="color:green;"><span class="glyphicon glyphicon-ok"></span></a>
                                @endif
                                @role('admin')
                                <a class="delete-icon pull-right icon" data-toggle="tooltip" data-placement="top"
                                   title="Delete" value="{{$task->id}}" style="color:red;cursor: pointer;"><span class="glyphicon glyphicon-trash edit" aria-hidden="true"
                                    ></span></a>

                                <a class="edit-icon pull-right icon" href="{{route('task.edit',$task->id)}}"
                                   data-toggle="tooltip" data-placement="top" title="Edit"><span class="glyphicon glyphicon-pencil delete"
                                                                                                 aria-hidden="true"></span></a>
                                @endrole


                            </div>
                            <table class="table table-striped mt-xl">
                                <tbody>
                                <tr>
                                    <th>
                                        Name
                                    </th>
                                    <td>
                                        {{$task->name}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Description
                                    </th>
                                    <td>
                                        {{$task->description}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <hr class="tall">
            </div>
        </div>
    </div>
@endsection