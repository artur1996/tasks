<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function token()
    {
        return $this->hasOne('App\UserActivations');
    }

    public function notifications()
    {
        return $this->belongsToMany('App\Notification', 'user_notification');
    }

    public function tasks()
    {
        return $this->belongsToMany('App\Task', 'user_task');
    }

    public function newNotifications()
    {
        return  $this->notifications()->where('read',false);
    }
}
