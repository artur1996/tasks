@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" id="panel">
                    <div class="panel-heading">Dashboard</div>

                    <div class="alert alert-danger">
                        Please confirm your account.
                    </div>
                    <div id="resend" class="text-center">
                        <button class="btn btn-success">Receive email again</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#resend').on("click", function () {
                $.ajax({
                    type: "POST",
                    url: "/resend_confirmation_email",
                    success: function(data) {
                        console.log(data.status);
                        $("#create").remove();
                        var div = document.createElement("div");
                            div.setAttribute("class","alert alert-"+data.status);
                            div.setAttribute("id","create");
                        document.getElementById("panel").appendChild(div);
                        var message =document.createTextNode(data.message);
                        div.appendChild(message);

                    }
                });
            });

        });
    </script>


@endsection
