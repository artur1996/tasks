@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <section class="panel">
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form action="{{ route('task.store') }}" class="form-horizontal form-bordered" method="POST" name="task"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-9">
                                    <div class="alert alert-info collapse">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Name</label>
                                <div class="col-md-6">
                                    <input type="text" value="" maxlength="100" class="form-control"
                                           name="name" required>
                                    @if ($errors->has('name'))
                                        <label class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </label>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Description</label>
                                <div class="col-md-6">
                                    <input type="text" value="" maxlength="100" class="form-control"
                                           name="description" required>
                                    @if ($errors->has('name'))
                                        <label class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </label>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Select Developer</label>
                                <div class="col-md-6">
                                    <select data-live-search="true" class="selectpicker" name="user_id">
                                        @foreach($developers as $develop)
                                            <option value="{{$develop->id}}">{{$develop->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-6">
                                    <button type="submit"
                                            class="btn btn-primary btn-lg mb-xlg">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection