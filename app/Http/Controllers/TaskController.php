<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use App\Task;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{

    public function __construct()
    {

    }

    const PROGRESS = 1;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::paginate(10);
        return view('task.index',[ 'tasks' => $tasks ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::where('name','developer')->first();
        $data = $role->users;
        return view('task.create',['developers' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task();
        $task->progress_id = self::PROGRESS;
        $task->name = $request->name;
        $task->description = $request->description;
        $task->save();
        DB::table('user_task')->insert([
            'user_id' => $request->user_id,
            'task_id' => $task->id
        ]);
        return redirect()->back()->with('status','Successfuly created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);
        return view('task.show',['task' => $task]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::where('id',$id)->firstOrFail();
        return view('task.edit',[ 'task' => $task ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if(Auth::user()->hasRole('admin')) {
            Task::where('id',$id)->update([
               'name' => $request->name,
                'description' => $request->description,
                'progress_id' => $request->progress
            ]);

        } else {
            Task::where('id',$id)->update([
                'progress_id' => $request->progress
            ]);
        }
        return response()->json('Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            Task::where('id',$id)->delete();
            DB::table('user_task')->where('task_id',$id)->delete();

        return response()->json(true);
    }

    public function myTasks() {
        $data = Auth::user()->tasks()->paginate(10);
        return view('task.index',['tasks' => $data]);
    }
}
