<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProgressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('progress')->insert([
            [
                'id' => 1,
                'name' => 'not started',
            ],
            [
                'id' => 2,
                'name' => 'progressing',
            ],
            [
                'id' => 3,
                'name' => 'completed'
            ]
    ]);
    }
}
