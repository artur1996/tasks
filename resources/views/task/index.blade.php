@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            {{--<div class="col-md-9 col-md-offset-2">--}}
            <div class="col-md-12">
                <div class="blog-posts">
                    @role('admin')
                    <a class="btn btn-primary" href="{{route('task.create')}}">Create Task</a>
                    @endrole
                    <hr>
                    {{$tasks->links()}}
                    @foreach ($tasks as $task)
                        <article class="post post-medium">
                            <div class="row">
                                <div class="col-md-3">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <div class="thumb-info mb-md">
                                                <img src="/img/!logged-user.jpg" class="rounded img-responsive"
                                                     alt="John Doe">
                                                <div class="thumb-info-title">
                                                    <a class="glyphicon-info-sign"><a href="{{ route('task.show',$task->id) }}">{{$task->name}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-9">
                                    <div class="icons">
                                        @if($task->belongs(\Illuminate\Support\Facades\Auth::user()))
                                            <a class="edit-icon pull-right icon" href="{{route('task.edit',$task->id)}}"
                                               data-toggle="tooltip" data-placement="top" title="Edit"><span class="glyphicon glyphicon-pencil edit"
                                                                                                             aria-hidden="true"></span></a>
                                        <a class="pull-right icon"
                                           data-toggle="tooltip" data-placement="top" title="Your task" style="color:green;"><span class="glyphicon glyphicon-ok"></span></a>

                                        @endif
                                        @role('admin')
                                        <a class="delete-icon pull-right icon" data-toggle="tooltip" data-placement="top"
                                           title="Delete" value="{{$task->id}}" style="color:red;cursor: pointer;"><span class="glyphicon glyphicon-trash edit" aria-hidden="true"
                                            ></span></a>
                                        @endrole
                                    </div>
                                    <div class="post-content">
                                        <h3>Name:</h3><span href="/task/{{$task->id}}">{{$task->name}}</span>
                                        <h3>Description:</h3><span>{{$task->description}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="post-meta">
                                        <span><i class="fa fa-calendar"></i> {{ Carbon\Carbon::parse($task->created_at)->format('M d, Y') }}</span>
                                    </div>
                                </div>
                            </div>
                        </article>
                    @endforeach
                    {{$tasks->links()}}
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.notify("Hello World");
        $(".delete-icon").click(function () {
            var id = $(this).attr('value');
            $.ajax({
                url: '/task/'+id,
                type: 'delete',
                dataType: "json",
                success: function (result) {
                    $(this).parents('article').remove();
                }
            });
        });

    });
</script>
@endsection